// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BDCLivenessKit",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "ios-liveness-sdk-package",
            targets: ["ios-liveness-sdk-package"]),
    ],
    targets: [
        .binaryTarget(name: "ios-liveness-sdk-package",
                      path: "BDCLivenessKit.xcframework")
    ]
)
